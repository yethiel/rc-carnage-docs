# Car Parameters

Paths (mesh, texture) are relative to the content directory. Internal `res://` paths are also possible to access resources that are included in the game.

Cars in RC Carnage use many features of the [VehicleBody](https://docs.godotengine.org/en/3.1/classes/class_vehiclebody.html) and [VehicleWheel](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html) nodes. References to used attributes are linked on this page. 

## File Format

Parameters use JSON as a format for car parameters. You can use the [example file](#example-file) to see how the syntax works.

## Coordinates

Coordinates work as follows:

- positive **x** axis: left 
- positive **y** axis: up
- positive **z** axis: forward.

This is different to Re-Volt and Blender. If you're porting a car from Re-Volt you have to invert the **x** and the **y** axis and divide all values by 100.

# Metadata

## name

*String*

Name of the car.

## author

*String*

Author of the car.

## description

*String*

Description to of the car to be shown in the menu.

## date

*String*

Date of creation.

## revision

*int* bigger than 0.

Revision or version number of the vehicle, usually incremented by one every time something has changed.

## type

*String*

Defines the type of vehicle (car, boat, plane, etc.)

Currently (19.0804) supported types:

- `vehicle/car`

---

# Car Behavior

## engine_force

*float*

The maximum engine force of the car.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclebody.html?highlight=vehiclebody#class-vehiclebody-property-engine-force)

## top_speed

*float*

At which speed the car stops accelerating.

## steer_angle

*float*

The maximum steer angle of all wheels.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclebody.html?highlight=vehiclebody#class-vehiclebody-property-steering)

## steer_speed

*float*

At which speed the wheels approach the maximum steer angle.

## jump_strength

*float*

Strength of the impulse when jumping.

## drift_force

*float*

Force that keeps the car going forwards when drifting.

---

# camera

## anchor

\[*float*, *float*, *float*]

Position relative to the car body that the camera follows. Usually it's between the front wheels.

---

# body

## mesh

*String*

Mesh of the car's body. 

## textures

[*String*]

List of textures of the car body. Faces of the PRM mesh that have the texture property 0 will use the first texture from the list. Faces with the property 1 will use the second texture from the list, etc.

## mass

*float*

Mass of the car body.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_rigidbody.html#class-rigidbody-property-mass)

## gravity_scale

*float*

How much of the global gravity is applied to the car.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_rigidbody.html#class-rigidbody-property-gravity-scale)

## offset

\[*float*, *float*, *float*]

Offset of the car mesh to the [VehicleBody](https://docs.godotengine.org/en/3.1/classes/class_vehiclebody.html?highlight=vehiclebody#vehiclebody) node.

## rotation

\[*float*, *float*, *float*]

Rotation of the mesh in radians.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_spatial.html#class-spatial-property-rotation)

## scale

\[*float*, *float*, *float*]

Scale of the mesh.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_spatial.html#class-spatial-property-scale)

## angular_damp_ground

*float*

Angular damping when the car is on the ground.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_rigidbody.html?highlight=rigidbody#class-rigidbody-property-angular-damp)

## angular_damp_air

*float*

Angular damping when the car is in the air.
[Reference](https://docs.godotengine.org/en/3.1/classes/class_rigidbody.html?highlight=rigidbody#class-rigidbody-property-angular-damp)

## friction

*float* between 0.0 and 1.0

The friction of the vehicle body when in contact with other objects.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_rigidbody.html?highlight=rigidbody#class-rigidbody-property-friction)

---

# wheels

## name

*String*

Name of the wheel (to be used in in-game editors).

## mesh

*String*

Mesh of the wheel.

## textures

[*String*]

List of textures of the wheel. Faces of the PRM mesh that have the texture property 0 will use the first texture from the list. Faces with the property 1 will use the second texture from the list, etc.

## position

\[*float*, *float*, *float*]

Position of the wheel.

## offset

\[*float*, *float*, *float*]

Offset of the wheel mesh from the actual position of the wheel.

## scale

\[*float*, *float*, *float*]

Scale of the wheel mesh.

## steer

*bool*

Whether or not the wheel steers.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-use-as-steering)

## powered

*bool*

Whether or not the wheel is powered by the engine.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-use-as-traction)

## radius

*float*

Radius of the wheel.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-wheel-radius)

## skid_width

*float*

Width of the skidmarks. This should be the width of the wheel mesh.

## rest_length

*float*

The distance in meters the wheel is lowered from its position.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-wheel-rest-length)

## grip

*float*

Determines how much grip a wheel has. 0.0 means no grip and 1.0 is normal grip.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-wheel-friction-slip)

## grip_drift

*float*

Grip when drifting.

## roll_influence

*float*

How much the wheel affects the roll of the car. Set it to 0.0 and the car will roll over more easily. Set it to 1.0 and the car will resist body roll.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-wheel-roll-influence)

## suspension_travel

*float*

The distance the suspension can travel.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-suspension-travel)

## suspension_stiffness

*float*

The stiffness of the suspension.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-suspension-stiffness)


## suspension_force

*float*

The maximum suspension force that the spring can resist. This should be three to four times higher than a quarter of the [body mass](#mass).

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-suspension-max-force)



## damping_compression

*float*

The damping applied to the spring when it is being compressed. It should be between 0.0 and 1.0. 

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-damping-compression)

## damping_relaxation

*float*

Similar to [damping_compression](#damping_compression), only that it's applied when the spring is relaxing. It should be slightly higher than damping_compression.

[Reference](https://docs.godotengine.org/en/3.1/classes/class_vehiclewheel.html#class-vehiclewheel-property-damping-relaxation)


---

# springs

Springs have their origin at a spot on the car body and point to one of the wheels. They can have an offset so that the springs end on the middle of the axle, for example. Springs have no effect on the car's behaviour and are only for decoration purposes.

## name

*String*

Display name of the spring.

## mesh

*String*

Mesh of the spring.

## textures

\[*String*]

List of textures of the spring.

## origin

\[*float*, *float*, *float*]

Origin point of the spring that is fixed to the car body.

## target_wheel

*int*

Index number of the wheel that the spring is connected to.

## target_offset

\[*float*, *float*, *float*]

Offset relative to the wheel that the spring is connected to.

## length

*float*

Length of the spring mesh.

---

# axles

Axles have their origin at one of the wheels and point to a spot on the car body. Axles have no effect on the car's behaviour and are only for decoration purposes.

## name

*String*

Display name of the axle.

## mesh

*String*

Mesh of the axle.

## textures

\[*String*]

List of textures of the axle.

## origin_wheel

*int*

Index number of the wheel that the axle is fixed to.

## target

\[*float*, *float*, *float*]

Target position on the car body that the axle points to.

---

# objects

A list of "car objects" that stick to the car and perform certain actions.

## name

*String*

Name of the object (to be shown in in-game editors).

## mesh

*String*

Mesh of the object.

## texture

*String*

Texture of the object.

## position

[*float*, *float*, *float*]

Position of the object, relative to the car body.

## scale

[*float*, *float*, *float*]

Scale of the object.

## rotation

[*float*, *float*, *float*]

Rotation of the object.

## actions

*list* of *String*

Which actions the object performs.

Currently (19.0804) supported actions:

- `rotate`

## action_axis

*dict* with *String* as key and [*float*, *float*, *float*] as value

Defines the axis on which actions are performed. For example,

```json
    "objects": {
        …
        "actions": ["rotate"],
        "action_axis": {
            "rotate": [0.0, 1.0, 0.0]
        }
    }
```

would rotate the object along its **y** axis.

---

## Example File

Here's an example file from 19.0804 of the car Enforcer:

```js
{
    "name": "Enforcer",
    "author": "flyboy",
    "description": "",
    "date": "2019-08-02",
    "revision": 0,
    "type": "vehicle/car",

    "engine_force": 22,
    "top_speed": 80.0,
    "steer_angle": 0.35,
    "steer_speed": 1.2,

    "body": {
        "mesh": "/cars/enforcer/body.prm",
        "texture": "/cars/enforcer/car.bmp",
        "mass": 9.6,
        "gravity_scale": 1.0,
        "offset": [0.0, -0.09, 0.0],
        "rotation": [0.0, 0.0, 0.0],
        "scale": [1.0, 1.0, 1.0]
    },

    "wheels": [
        {
            "name": "wheelfl",
            "mesh": "/cars/enforcer/wheell.prm",
            "texture": "/cars/enforcer/car.bmp",
            "position": [0.23, 0.0, 0.49],
            "offset": [0.0, 0.0, 0.0],
            "scale": [1.0, 1.0, 1.0],
            "steer": true,
            "powered": true,
            "radius": 0.12,
            "skid_width": 0.05,
            "rest_length": 0.06,
            "grip": 1.4,
            "roll_influence": 0.05,
            
            "suspension_travel": 0.08,
            "suspension_stiffness": 80.47,
            "suspension_force": 10000.0,

            "damping_compression": 0.93,
            "damping_relaxation": 2.92
        },
        {
            "name": "wheelfr",
            "mesh": "/cars/enforcer/wheelr.prm",
            "texture": "/cars/enforcer/car.bmp",
            "position": [-0.23, 0.0, 0.49],
            "offset": [0.0, 0.0, 0.0],
            "scale": [1.0, 1.0, 1.0],
            "steer": true,
            "powered": true,
            "radius": 0.12,
            "skid_width": 0.05,
            "rest_length": 0.06,
            "grip": 1.4,
            "roll_influence": 0.05,
            
            "suspension_travel": 0.08,
            "suspension_stiffness": 80.47,
            "suspension_force": 10000.0,

            "damping_compression": 0.93,
            "damping_relaxation": 2.92
        },
        {
            "name": "wheelbl",
            "mesh": "/cars/enforcer/wheell.prm",
            "texture": "/cars/enforcer/car.bmp",
            "position": [0.23, 0.0, -0.40],
            "offset": [0.0, 0.0, 0.0],
            "scale": [1.0, 1.0, 1.0],
            "steer": false,
            "powered": true,
            "radius": 0.12,
            "skid_width": 0.05,
            "rest_length": 0.06,
            "grip": 1.5,
            "roll_influence": 0.05,
            
            "suspension_travel": 0.08,
            "suspension_stiffness": 80.47,
            "suspension_force": 10000.0,

            "damping_compression": 0.93,
            "damping_relaxation": 2.92
        },
        {
            "name": "wheelbr",
            "mesh": "/cars/enforcer/wheelr.prm",
            "texture": "/cars/enforcer/car.bmp",
            "position": [-0.23, 0.0, -0.40],
            "offset": [0.0, 0.0, 0.0],
            "scale": [1.0, 1.0, 1.0],
            "steer": false,
            "powered": true,
            "radius": 0.12,
            "skid_width": 0.05,
            "rest_length": 0.06,
            "grip": 1.5,
            "roll_influence": 0.05,
            
            "suspension_travel": 0.08,
            "suspension_stiffness": 80.47,
            "suspension_force": 10000.0,

            "damping_compression": 0.93,
            "damping_relaxation": 2.92
        }
    ],
    "objects": {
        "name": "key",
        "mesh": "",
        "texture": "",
        "position": [0.0, 0.0, 0.0],
        "scale": [1.0, 1.0, 1.0],
        "rotation": [0.0, 0.0, 0.0],
        "actions": ["rotate"],
        "action_axis": {
            "rotate": [0.0, 1.0, 0.0]
        }
    }
}
```