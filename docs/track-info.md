# Track Information

Each track can have a track information file (trackname.json) that contains metadata for the game.

## startpos

Offset of the startgrid.

## startrot

The horizontal rotation of the start position. (Only works when no start_grid is provided)

---

# start_grid

List of dictionaires that contain information spots for cars:

## position

Position of the spot relative to the startpos offset.

## rotation

Rotation of the spot in radians.

---

# zones

List of level zones that can be used by game modes.

## type

Currently only `out_of_bounds` is supported.

## position

Position of the zone's center.

## size

Extent of the zone in halves.

```json
{
    "zones": [
        {
            "type": "out_of_bounds",
            "position": [0.0, -10.0, 0.0],
            "size": [100.0, 5.0, 200.0]
        }
    ],
    "startpos": [0, 0, 0],
    "startrot": 0, 
    "start_grid": [
        {
            "position": [-1, -14, 0],
            "rotation": 0.25
        },
        {
            "position": [1, -14, 0],
            "rotation": 0.75
        }
    ]
}
```