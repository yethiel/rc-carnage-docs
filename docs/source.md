# Source Code

## Game

The source code of the game is available at [https://gitlab.com/yethiel/rc-carnage](https://gitlab.com/yethiel/rc-carnage).

## Documentation

The source of this site is available at [https://gitlab.com/yethiel/rc-carnage-docs](https://gitlab.com/yethiel/rc-carnage-docs).