# Settings

The settings file is located in the user directory:

**Linux**: `~/.local/share/rc-carnage/settings.json`

**macOS**: `~/Library/Application Support/rc-carnage/settings.json`

**Windows**: `%APPDATA%\Godot\app_userdata\rc-carnage\settings.json`

Depending on the way you installed the game, you might have to check the settings tab to find out the actual path.

## content_dir

Folder for game content that includes RC Carnage cars and tracks. You can select a Re-Volt directory for loading Re-Volt levels, too.

## game_mode

Game mode the game starts in. Currently only `lcs` (Last Car Standing) and `free` (free roam) are supported.

## level

The selected level to load.

# playerX

Player specific settings.

## vehicle

Selected vehicle for the player.

# video

## draw_skidmarks

*true* or *false*

Whether to draw skidmarks behind cars.

## width, height

*int*

Set to -1 for automatic resoltion or to any number > 0 to set the resolution.

## light

*true* or *false*

Whether to use lights.

## special_textures

*true* or *false*

Whether special textures are loaded (normal, roughness, metallic, ambient occlusion, ...). Might have an impact on performance if turned on.

# audio

## music_enabled

*true* or *false*

Whether music plays.