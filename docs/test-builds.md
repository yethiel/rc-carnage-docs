# 19.0821


**Changes**:

- Car stunt, jump and drift mechanics: [https://rccarnage.com/docs/controls/](https://rccarnage.com/docs/controls/)
- Special textures for cars and tracks
- More car parameters [https://rccarnage.com/docs/car-parameters/](https://rccarnage.com/docs/car-parameters/)
- Gator Raid by DC.All with springs and axles
- Engine sound
- Soundtrack by ZetaSphere and Qvarcos
- LMS game mode on Temple 2, Test track N64 Block Fort
- Potato mode for slow PCs [https://rccarnage.com/docs/launching/#video-driver](https://rccarnage.com/docs/launching/#video-driver)

[**Download** (linux32, linux64, win32, win64, macos)](https://garage.re-volt.io/s/McDrbLRfpZMANBH)


# Old Builds


## 19.0804

```markdown
RC Carnage Test Build
=====================

Features
--------
- Controller support (one controller + keyboard)
- Rumble (when drifting)
- Split screen (2, vertical)
- RV Files:
    - Cars models and textures
    - Track .inf, .w and textures
- Skidmarks

You can just press any button on a controller and then press on the Start button in the main menu.
Press Enter on the keyboard and on a controller and you'll start the game in split screen automatically.

Controls
--------
The controller keymap follows conventions. Start opens the menu.

Keyboard: 
- Arrow keys: Control car
- Esc: menu
- Control + R: Reload car (single player only)

Credits
-------
- Car (Enforcer) by flyboy

Known Issues
------------
- Cars cannot be flipped without going back to menu and restarting
- Collision is generated from the track mesh, .ncp is ignored
- Transparency doesn't work
```


[Download (linux32, linux64, win32, win64, macos)](https://garage.re-volt.io/s/cfRJTbGkasZzcAR)

## 19.0719

Features:

- Controller support (one controller + keyboard)
- Rumble (when drifting)
- Split screen (2, vertical)

You can just press any button on a controller and then press on the Start button in the main menu.
Press Enter on the keyboard and on a controller and you'll start the game in split screen automatically.

Fixes:

- The content folder should now be correctly recognized on macOS



[Download (linux, macos, win32, win64)](https://garage.re-volt.io/s/2fotPEoCj2648L7)
