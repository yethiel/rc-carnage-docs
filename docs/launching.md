# Launching

## Video Driver

If you're experiencing performance issues, start the game with the `--video-driver GLES2` option. This will start the game with the GLES2 video driver instead of GLES3 and it will also disable most graphical features that might impact performance.