# Controls

# Keyboard

**Accelerate**: Up arrow  
**Reverse**: Down arrow  
**Left**: Left arrow  
**Right**: Right arrow

**Flip car**: End  
**Jump**: Space  
**Perform stunts**: Hold space and use arrow keys  
**Drift**: Hold shift while steering.

**Menu**: Escape  

## Debug

**Reload car**: Control + R  
**Car camera**: F1  
**Free camera**: F6  

## Free Camera

**Forward**: E  
**Backward**: Q  
**Up**: W  
**Down**: S  
**Left**: A  
**Right**: D

# Controller

We'll call the A/B/X/Y buttons North, East, South and West buttons instead since they're different on every controller.

**Accelerate**: Right trigger  
**Reverse**: Left trigger  
**Left**: Left stick left  
**Right**: Left stick right

**Flip car**: East button  
**Jump**: Press right stick  
**Perform stunts**: Right stick  
**Drift**: Hold Left bumper while steering

**Menu**: Start